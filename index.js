var express = require('express');
var app = express();
//CODE AJOUTÉ
var pausedUsers = {}

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

//CODE AJOUTÉ
//app.post('/pause', jsonParser, function (req, res) {
//  const userId = req.body.userId
//  const paused = req.body.paused
//  pausedUsers[userId] = paused
//  res.send('ok')
//})

console.log('paused user:' + pausedUsers)